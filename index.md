---
title: Arxiki
banner_image: "/img/banner.png"
layout: landing-page
heading: Dimibiol
partners:
- "/uploads/2017/11/13/stem.png"
- "/uploads/2017/11/13/UPenn_logo.png"
- "/uploads/2017/11/13/nysed.png"
services:
- heading: test
  description: test description
  icon: "/uploads/2017/11/13/nysed.png"
- description: Performing collaborative research and providing services to support
    the Health Sector.
  heading: Health
  icon: "/uploads/2017/11/13/health.png"
- description: Performing collaborative research and providing services to support
    the biotechnology sector.
  heading: BioTech
  icon: "/uploads/2017/11/13/biotech.png"
sub_heading: Biology
textline: "    <b>blablalblalblalballbalbla</b>"
hero_button:
  text: Learn more
  href: "/about"
show_news: true
show_staff: false
menu:
  navigation:
    identifier: _index
    weight: 1

---
